//
//  FailedBankInfo.m
//  FailedBankCD
//
//  Created by Lappie on 1/25/15.
//  Copyright (c) 2015 FBCD. All rights reserved.
//

#import "FailedBankInfo.h"


@implementation FailedBankInfo

@dynamic city;
@dynamic name;
@dynamic state;
@dynamic details;

@end
