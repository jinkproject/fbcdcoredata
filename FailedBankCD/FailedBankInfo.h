//
//  FailedBankInfo.h
//  FailedBankCD
//
//  Created by Lappie on 1/25/15.
//  Copyright (c) 2015 FBCD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "FailedBankDetails.h"


@interface FailedBankInfo : NSManagedObject

@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * state;
@property (nonatomic, retain) FailedBankDetails *details;

@end
