//
//  FailedBankDetails.m
//  FailedBankCD
//
//  Created by Lappie on 1/25/15.
//  Copyright (c) 2015 FBCD. All rights reserved.
//

#import "FailedBankDetails.h"
#import "FailedBankInfo.h"


@implementation FailedBankDetails

@dynamic closeDate;
@dynamic updateDate;
@dynamic zip;
@dynamic info;

@end
