//
//  FBCDMasterViewController.h
//  FailedBankCD
//
//  Created by The Beast on 1/25/15.
//  Copyright (c) 2015 FBCD. All rights reserved.
//
#import "FailedBankInfo.h"
#import <UIKit/UIKit.h>
@interface FBCDMasterViewController : UITableViewController
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSArray * failedBankInfos;
@end
